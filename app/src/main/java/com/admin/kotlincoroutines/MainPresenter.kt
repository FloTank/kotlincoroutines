package com.admin.kotlincoroutines

import android.util.Log
import de.appsfactory.mvplib.presenter.MVPPresenter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainPresenter : MVPPresenter() {

    // waits for Backend to send Token
    fun requestToken(): Token {
        return Token()
    }

    // waits for Backend to send Post
    fun getPostFromBackend(token: Token): Post {
        return Post(token)
    }

    fun processPost(post: Post) {
        Log.d("xxxx", "processed")
    }


    // Normal Thread
    // doens't handle exceptions on its own
    fun normalThread() {
        val token = requestToken() // -> Block Thread until Token received
        val post = getPostFromBackend(token) // -> Block Thread until Post received
        processPost(post)
    }


    // MVP-Lib -> Using Callbacks, which can lead to callback hell
    // -> Handles Exceptions
    fun doInBackground() {
        doInBackground(1) {
            requestToken()
        }.addOnSuccess { token ->

            doInBackground(3) {
                getPostFromBackend(token)
            }.addOnSuccess { post ->
                processPost(post)
            }.addOnError { e ->
                e.printStackTrace()
            }.execute()

        }.addOnError {
            it.printStackTrace()
        }.execute()
    }


    // RxJava -> Also leads to Callback Hell and is Overkill, moreover a lot of Combinators which you have to remember
    // Handles Exceptions
    private val backendThread = Schedulers.io()
    private val mainThread = AndroidSchedulers.mainThread()
    private fun requestTokenRx(): Single<Token> {
        return Single.fromCallable { Token() }
    }

    private fun getPostFromBackendRx(token: Token): Single<Post> {
        return Single.fromCallable { Post(token) }
    }

    fun rxJavaAsync() {
        requestTokenRx()
                .subscribeOn(backendThread)
                .observeOn(mainThread)
                .subscribe { token ->
                    getPostFromBackendRx(token)
                            .subscribeOn(backendThread)
                            .observeOn(mainThread)
                            .subscribe { post ->
                                processPost(post)
                            }
                }
    }
}