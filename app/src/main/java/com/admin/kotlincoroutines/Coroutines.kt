package com.admin.kotlincoroutines

import android.util.Log
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.UI

class Coroutines {
    // suspends the thread until the Backend send the token -> waits until token received
    suspend fun requestToken(): Token {
        return Token()
    }

    // suspends until post received
    suspend fun getPostFromBackend(token: Token): Post {
        return Post(token)
    }

    fun processPost(post: Post) {
        Log.d("xxxx", "processed")
    }


    // suspended functions
    // DOES NOT HANDLE EXCEPTIONS BY ITSELF
    suspend fun coroutinesSuspended() {
        val token = requestToken()
        val post = getPostFromBackend(token)
        processPost(post)
    }

    // PROBLEM 1: You will come to a point where you cant suspend anymore -> onCreate()
    fun notSuspendedFunction() { // Can NOT suspend the Execution of the function
        val token = requestToken() // -> Can Suspend Execution of the function
        val post = getPostFromBackend(token)
        processPost(post)
    }

    // SOLUTION: Coroutine Builders
    fun solutionForNotSuspendingFunction() {
        // fire and forget Coroutine
        // return immediately
        // works on itself in background, just like Background Thread -> processPost also in Background
        launch {
            val token = requestToken()
            val post = getPostFromBackend(token)
            processPost(post) // -> Also on Background Thread
        }
    }

    // PROBLEM: How to Update UI then?!
    fun launchAndUpdateUi() {
        // pass Coroutines UI Context to dispatch execution to UI
        launch(UI) {
            val token = requestToken()
            val post = getPostFromBackend(token)
            processPost(post) // will now get executed on UI Thread
        }
    }


    // PROBLEM:
    // What if i don't want to define a suspended function everytime or
    // What if I want to make a normal function asynchronous?

    //Async creates Deffered object, which can be asynchronously awaited later on
    suspend fun executeAsynchronousFunctionWithAsync() {
        val one = async { doSomethingUsefulOne() }
        val two = async { doSomethingUsefulTwo() }

        val oneResult = one.await()

        println("The answer is $oneResult ${two.await()}")
    }

    // WithContext suspends the defined block, waits and returns the result
    suspend fun executeAsynchronousFunctionAsyncWithContext() {
        val one = withContext(DefaultDispatcher) {
            doSomethingUsefulOne()
        }
        val two = withContext(DefaultDispatcher) {
            doSomethingUsefulTwo()
        }

        println("The answer is $one $two")
    }

    fun exectuteAsync() {
        launch {
            executeAsynchronousFunctionWithAsync()
            executeAsynchronousFunctionAsyncWithContext()
        }
    }

    private fun doSomethingUsefulOne(): String = "One"
    private fun doSomethingUsefulTwo(): String = "Two"

    //EXAMPLE FOR PROGRESSBAR

    fun example() {
        var showProgressBar = false
        launch(UI) {
            showProgressBar = true
            requestToken()
            showProgressBar = false
        }
    }
}