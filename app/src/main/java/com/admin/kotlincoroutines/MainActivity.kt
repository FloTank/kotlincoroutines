package com.admin.kotlincoroutines

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.admin.kotlincoroutines.databinding.ActivityMainBinding
import de.appsfactory.mvplib.view.MVPActivity

class MainActivity : MVPActivity<MainPresenter>() {

    override fun createPresenter(): MainPresenter {
        return MainPresenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.presenter = mPresenter
    }
}
